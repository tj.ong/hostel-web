export class Room {
    _id:string;
    roomno: number;
    roomtype: string;
    rentalrate: number;
    block: string;
    student: string;
}
